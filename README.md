# zabbix to http gateway #
Enable a zabbix agent to communicate with zabbix server through an HTTP proxy

### What is this repository for? ###

You may access a zabbix server when zabbix agent runs behind an HTTP proxy. This is not normally feasible since the zabbix protocol is not HTTP. 

Notes:
* zabbix agent must work in active mode
* this is a very simple gateway, may or may not work for you

### Installation ###
Installation for systems with systemd. For system wih init bash scripts create your start script.

* Copy zabbix2http.py in the same host as zabbix agent, copy in /usr/bin/
* Copy zabbix2http.service to /etc/systemd/system
* Run systemctl daemon-reload as root
* Configure in zabbix_agentd.conf : ServerActive: 127.0.0.1
* Put http2zabbix.php in zabbix server accessible e.g.on /http2zabbix.php

* Edit http2zabbix.php and specify your zabbix server if needed
* Edit zabbix2http.py and 
  * specify your proxy address. Use IP.
  * specify your zabbix http url
* Start zabbix2http with "systemctl start zabbix2http"

![picture](schematic.png)

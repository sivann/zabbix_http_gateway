#!/usr/bin/python

###################################################################

# Accessible HTTP proxy
proxyHost="1.2.3.4"
proxyPort=3128

# URL with  http2zabbix.php, probably inside zabbix server host . 
# Must be accessible by proxy.intranet.com
http2zabbix_url="http://zabbixserver.example.com/http2zabbix.php"

# Configure agent to connect to this port
listenPort=10051

###################################################################
###################################################################
###################################################################
import select
import socket
import httplib, urllib
import time

server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
server_socket.bind(('', listenPort))
server_socket.listen(10)

print("Listening on port %d"%listenPort)

read_list = [server_socket]
def httpPost(data):
    params = urllib.urlencode({'agent-data': data})
    headers = {"Content-type": "application/x-www-form-urlencoded", "Accept": "text/plain"}
    print('Connecting to %s:%s'%(proxyHost,proxyPort))
    conn = httplib.HTTPConnection(proxyHost, proxyPort, timeout=10)
    print('Sending request')
    try:
        conn.request("POST", http2zabbix_url, params, headers)
    except socket.error, exc:
        print('Error: proxy(%s:%s) http2zabbix:(%s):%s\n',(proxyHost,proxyPort,http2zabbix_url,exc))
        time.sleep(0.5)
        return False
        
    response = conn.getresponse()
    print response.status, response.reason
    response_data = response.read()
    return response_data

while True:
    readable, writable, errored = select.select(read_list, [], [])
    for s in readable:
        if s is server_socket:
            client_socket, address = server_socket.accept()
            read_list.append(client_socket)
            print "Connection from", address
        else:
            # Receive data from agent
            data = s.recv(102400)
            if data:
                print('Received from agent:%d bytes'%len(data))
                # Send data to server
                try:
                    response = httpPost(data)
                    if (len(response)) > 0 and response.find("success") > 0:
                        print('HTTP success response:%d bytes'%len(response))
                    else:
                        print('HTTP non-success response:%s'%response)
                except socket.timeout, exc:
                    print('Socket timeout posting data to proxy')
                    s.close()
                    read_list.remove(s)
                    continue

                # send server response to agent
                s.sendall(response);
            else:
                s.close()
                read_list.remove(s)
                print "Connection closed"



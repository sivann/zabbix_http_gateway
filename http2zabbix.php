<?php

$zabbix_server="localhost";
$zabbix_port=10051;

if (file_exists("http2zabbix.conf.php")) {
    include ("http2zabbix.conf.php");
}

/* *************************
 * Do not change code below
 * *************************
 */

openlog("http2zabbix", LOG_PID | LOG_PERROR, LOG_LOCAL0);

$fp = fsockopen($zabbix_server, $zabbix_port, $errno, $errstr, 30);

if (!$fp) {
    syslog(LOG_ERR, "cannot fsockopen {$zabbix_server }:{$zabbix_port} error: $errstr");
} elseif (isset($_POST['agent-data'])) {
    $data=$_POST['agent-data'];
    fwrite($fp, $data);

    while (!feof($fp)) {
        $s = fgets($fp, 1024);
        echo $s;
    }
    fclose($fp);
}
else {
    echo "Hello there, I expected more from you.";
}
